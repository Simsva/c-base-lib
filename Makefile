include ./flags.mk

SRC=$(wildcard src/*.c) $(wildcard src/**/*.c)
SRC_MK=$(SRC:.c=.d)
OBJ=$(SRC:.c=.o)
OUT=test
DOCS=docs/

BUILDFILES=$(OBJ) $(SRC_MK) $(OUT)

all: $(OBJ) $(OUT)

clean:
	@echo "Cleaning buildfiles"
	@rm -f $(BUILDFILES)
	@rm -rf $(DOCS)

%.o: %.c
	@echo "CC	$(shell basename $@)"
	@$(CC) -o $@ -c $< $(CPPFLAGS) $(CFLAGS)

$(OUT): $(OBJ)
	@echo "LD	$(shell basename $@)"
	@$(CC) -o $@ $(CFLAGS) $^ $(LDFLAGS)

docs:
	@echo "Generating documentation"
	@$(DOXYGEN) >/dev/null || echo "Error while generating documentation!"

.PHONY: all clean docs
