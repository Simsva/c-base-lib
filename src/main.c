/*
 * Copyright 2023 Emma Ericsson
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS”
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file
 * @author Emma Ericsson <emma@krlsg.se>
 *
 * @brief Testing suite for the entire library
 */
#include <stdio.h>
#include <utils/vector.h>

static inline void vec_dump_info(vec_t *vec) {
    printf("vec->sz: %zu\nvec->capacity: %zu\n",
           vec->sz, vec->capacity);
}

int main(void) {
    /* pretend that this is doing structured tests on each and every function */

    vec_t vec;
    vec_init(&vec, 1);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)1);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)2);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)3);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)4);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)5);
    vec_dump_info(&vec);
    vec_push(&vec, (vec_item_t)6);
    vec_dump_info(&vec);

    for(size_t i = 0; i < 134; ++i) vec_push(&vec, (vec_item_t)(i+7));
    vec_dump_info(&vec);

    vec_item_t arr[] = { (void *)1, (void *)2, (void *)3 };
    vec_extend_exact(&vec, arr, 3);
    vec_dump_info(&vec);
    vec_extend(&vec, arr, 3);

    printf("heheheh\n");
    vec_dump_info(&vec);
    vec_debug_dump_ptr(stdout, &vec);
    putchar('\n');

    vec_delete(&vec, 4);

    vec_dump_info(&vec);
    vec_debug_dump_ptr(stdout, &vec);
    putchar('\n');

    vec_insert(&vec, 4, (vec_item_t)6);

    vec_dump_info(&vec);
    vec_debug_dump_ptr(stdout, &vec);
    putchar('\n');

    vec_debug_dump_ptr(stdout, &vec);
    putchar('\n');
}
