# C Base Library

Implementations of various useful data structures and other things in C.

## Usage

Copy the needed files into another project. All source files are located in the
`src/` directory and all header files are located in the `include/` directory.
Do not remove any license text from the copied files.

## Directory Structure

All source files and internal header files should lie in the `src/` directory,
and all external header files should lie in the `include/` directory. These
directories should be divided into categories themselves, and no files except
the `main.c` file should be placed in the root. A list of subdirectories
follows:

| Path    | Description            |
|---------|------------------------|
| `utils` | Common data structures |

The previously mentioned `main.c` file should be able to run tests on all other
code in the repo. The structure of these tests has not yet been decided.

## Conventions

All code should follow these conventions.

### Code Style

- Follow `.editorconfig`! This should be automatic if you use a sane editor.
- Comments should be C-style (i.e. `/* */`, not `//`).
- Lines of code or text should not exceed 80 characters.
 + With the exception of Markdown tables (and probably more things I forgot).
- Indentation should be 4 spaces wide.
- Function, variable, constant, file-, and type names should be in snake_case.
- Macro names should be in SCREAMING_SNAKE_CASE.
- Functions and all statements should have the opening brace on the same line.
- The star in pointer types should be placed with the variable name, not the
  type.
  + Example: `char *str;`
- `#endif`s paired with `#ifdefs` should be followed with a comment of the macro
  being checked for.
  + Example: `#endif /* DEBUG */`
  
For more specific guidelines check the sections below.

### Documentation and Copyright

Documentation should be written in Doxygen format using C-style comments in
every external header file (i.e. every header file in the `include/` directory).
Every header file should also include a file comment describing what it exposes.
All files, no matter if they are headers, should include a copy of the license
at the top of the file. When both a documentation of a file and a license is
present in a file, the license should be placed first.

The corresponding definition of every public function in a source file should
copy the documentation from the external header file using the Doxygen directive
`@copydoc`. For example: `/** @copydoc some_function */`. Any internal comment
about the function should be placed before the Doxygen comment. All source files
should also have file comments and if the source files perfectly corresponds
with one header file the documentation of that header file can be copied using
`@copydoc`. This also copies the documentation of all functions, so the
`@copydoc` directive can be skipped on functions in this case.

An example of acceptable documentation for an example `list.h` follows. This can
also be used as a reference for other formatting, such as whitespace. (Note that
Doxygen removes the example comments from this file, so for accurate information
read it on GitLab or in a text editor.)

```c
/*
 * Copyright 2023 John Doe
 * 
 * ...
 */

/**
 * @file
 * @author John Doe <john.doe@example.com>
 * 
 * @brief Implmentation of a basic doubly linked list type.
 */
#ifndef CBASE_UTILS_LIST_H_
#define CBASE_UTILS_LIST_H_
 
/**
 * @brief A node in a linked list
 */
typedef struct list_node {
    /** Pointer to the next node in the list */
    struct list_node *next;
    /** Pointer to the previous node in the list */
    struct list_node *prev;
    /** Value of the node */
    void *value;
} list_node_t;
 
/**
 * @brief A linked list
 */
typedef struct list {
    /** Head and tail of the list */
    list_node_t *head, *tail;
    /** Size of the list */
    size_t sz;
} list_t;

/**
 * @brief Initializes a linked list
 *
 * @param[out] list   List to initialize
 * @return Initialized list
 */
list_t *list_init(list_t *list);

#endif /* CBASE_UTILS_LIST_H_ */
```

Using `the` in parameter, return value, or struct member documentation should
generally be avoided as it is redundant. The documentation for functions should
describe what they do, not what they should do, i.e. `Initializes list` instead
of `Initialize list`.

### Headers

Headers should use header guards when necessary, not `#pragma once`. The name of
the header guard macro should be `CBASE` followed by the directory and lastly
the filename followed by an underscore. For example: `CBASE_UTILS_LIST_H_`.

### Structs

Struct names should not include the `_t` suffix, which should be reserved for
typedefs. All externally visible structs should be defined in the following way
with a typedef:

```c
typedef struct name {
    int a, b, c;
} name_t;
```

All functions acting on a struct `<struct>` should be named `<struct>_something`
and generally take a pointer to the struct as the first argument. Every
underscore indicates a new category of functions, with the most important
information placed to the left. For example `list_item_push` and `list_item_pop`
are correct, while `list_push_item` and `list_pop_item` are not. In general the
names of most functions with similar functionality should match as much as
possible from the left.

The following function names are reserved, but they do not need to be
implemented when they aren't relevant or necessary:

| Function name        | Description                                                                   |
|----------------------|-------------------------------------------------------------------------------|
| `<struct>_init_*`    | Should initialize a provided struct pointer. Returns a pointer to the struct. |
| `<struct>_new_*`     | Should allocate and initialize a struct on the heap.                          |
| `<struct>_destroy_*` | Should free all struct members, but not the struct itself. Returns void.      |
| `<struct>_free_*`    | Should destroy the struct and free it. Returns void.                          |

### Enums

Enum names should not include the `_t` suffix, which should be reserved for
typedefs. Enums should in general not be typedefed like structs, but this is not
strictly prohibited. All enum values should be in SCREAMING_SNAKE_CASE and
should generally start with a common prefix.
